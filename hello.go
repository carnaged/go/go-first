package main

import (
	"fmt"
)

// func main(){

// Basic variable declaration

	// var a int
	// var b int = 6
	// c := 7  // Same as var c int = 7
	// fmt.Println(a,b,c, "Hello World")

// If and its family

	// if c > 5 {
	// 	fmt.Println("c is big")
	// }else if c < 2 {
	// 	fmt.Println("C is small")
	// }else {
	// 	fmt.Println("C is inbetween")
	// }

// Arrays

	// var a[10] int
	// // fmt.Println(a)
	// a[2] = 4
	// fmt.Println(a)

	// b := [3] int {5,3,7}
	// fmt.Println(b)

// Slices

	// c := [] int {1,2,3,4}
	// var d[] int
	// fmt.Println(c)
	// fmt.Println(append(c,5))
	// d = append(c,5,6)
	// fmt.Println(d)
	// d = append(d,7)
	// fmt.Println(d)
	
// Maps
		// // map[type_of_keys]type_of_values

		// map_a := make(map[string]int)

		// map_a["Josh"] = 23
		// map_a["Bob"] = 19
		// map_a["Katie"] = 44

		// fmt.Println(map_a)
		// fmt.Println(map_a["Bob"])

		// delete(map_a, "Bob")
		// fmt.Println(map_a)

// Loops

		// for i := 0; i<10; i++ {
		// 	fmt.Println(i)
		// }
		// // As a while loop
		// j := 0
		// for j<5{
		// 	fmt.Println(j, "as a while loop")
		// 	j++
		// }

		// // Working with arrays:
		// arr := [5] int {5,4,3,2,1}
		// for index, value := range arr {
		// 	fmt.Println(index,":",value)
		// }

		// // Working with maps
		// mapp := make(map[string]int)
		// mapp["one"] = 1
		// mapp["two"] = 2
		// mapp["three"] = 3

		// for key, value := range mapp {
		// 	fmt.Println(key, ":", value)
		// }

// Functions
	
	// c := mult(2,3)
	// fmt.Println(c)

	// res,size := mult_ret(10)
	// fmt.Println(res,size)
// }

// func mult(a int, b int) int {
// 	return a*b
// }

// func mult_ret(a int) (int, string){
// 	if a < 10{
// 		return 1, "Big"
// 	}else {
// 		return 0,"Small"
// 	}
// }

//Structures
	// //needs to be outside functions
	// type person struct{
	// 	name string
	// 	age int
	// }
	
	// func main() {
	// 	a := person{name:"Bob", age:9}
	// 	fmt.Println(a)
	// 	fmt.Println(a.name)
		
	// }

// Pointers

func main() {
	a := 5
	fmt.Println(a, &a)
	incrementer(&a)
	fmt.Println(a)
}

func incrementer(x *int){
	*x++
}
