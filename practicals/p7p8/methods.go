package main

import "fmt"

const usixteenbitmax float64 = 65535
const kmhMultiple float64 = 1.60934

type car struct {
	gaspedal      uint16 // 0 - 65535
	brakepedal    uint16
	steeringwheel int16
	topspeedkmh   float64
}

func (c car) kmh() float64 {
	return float64(c.gaspedal) * (c.topspeedkmh / usixteenbitmax)
}
func (c car) mph() float64 {
	return float64(c.gaspedal) * (c.topspeedkmh / usixteenbitmax / kmhMultiple)
}

func (c *car) newTopSpeed(newspeed float64) {
	c.topspeedkmh = newspeed
}

func main() {
	acar := car{22000, 0, 12221, 100.0}
	bcar := car{
		gaspedal:      234,
		brakepedal:    0,
		steeringwheel: 3,
		topspeedkmh:   220.0}
	fmt.Println(acar)
	fmt.Println("Hello ", bcar.gaspedal)
	fmt.Println("Kmh:", acar.kmh())
	acar.newTopSpeed(400)
	fmt.Println("Kmh:", acar.kmh())
	fmt.Println("Kmh:", acar)

}
