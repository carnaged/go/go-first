package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

// SiteMapIndex contains the decoded xml
type SiteMapIndex struct {
	Locations []Location `xml:"sitemap"`
}

// Location string from xml doc
type Location struct {
	Loc string `xml:"loc"`
}

func (l Location) String() string {
	return fmt.Sprintf(l.Loc)
}
func main() {
	resp, _ := http.Get("https://www.washingtonpost.com/news-sitemaps/index.xml")
	bytes, _ := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	var s SiteMapIndex
	xml.Unmarshal(bytes, &s)
	// fmt.Println(s.Locations)
	for _, Location := range s.Locations {
		fmt.Printf("%s", Location)
	}
}
