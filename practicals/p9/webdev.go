package main

import (
	"fmt"
	"net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Tips</h1>")
	fmt.Fprintf(w, "<p>You can put html here<p>")
	fmt.Fprintf(w, "<p>and even use %s<p>", "string formatting")
	fmt.Fprintf(w, `multiline is also
	possible`)

}

func main() {
	http.HandleFunc("/", indexHandler)
	http.ListenAndServe(":8000", nil)
}
