package main

import "fmt"

type car struct {
	gaspedal      uint16 // 0 - 65535
	brakepedal    uint16
	steeringwheel int16
	topspeedkmh   float64
}

func main() {
	acar := car{22000, 0, 12221, 100.0}
	bcar := car{
		gaspedal:      234,
		brakepedal:    0,
		steeringwheel: 3,
		topspeedkmh:   220.0}
	fmt.Println(acar)
	fmt.Print(bcar.gaspedal)
}
